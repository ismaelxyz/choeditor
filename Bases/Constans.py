def path(fl, *kw):
    fl = __import__("os").path.dirname(fl)

    if kw:
        return __import__("os").path.join(fl, *kw)

    return fl
    
def confirmar(**kw):
    for x in kw:
        if not kw[x]:
            return False
    return True
TMP = __import__("os").environ["TMP"]