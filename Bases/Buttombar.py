#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Ismael Belisario <ismaelbeli.com@gmail.com>
# Nombre Clave: BUTTOM
# Copyright © 2020 Ismael Belisario
# Este archivo probiene de: https://github.com/ismaelxyz

"""
Copyright © 2020 Ismael Belisario

This file is part of Choeditor.

Choeditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Choeditor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Choeditor.  If not, see <https://www.gnu.org/licenses/>.
"""

from Baseswigets import forWigets, BaseBars

__version__ = "1.0"
__author__ = "Ismael Belisario"
__namep__ = "BTTOM"
__license__ = "GNU GPL 3 or later version"


class ButtomBar(BaseBars):

	def __init__(self, cuerpo, theme, **kw):
		super().__init__(cuerpo, theme, **kw)
	
	def create(self):
		lis = ["Python", "UTF-8", "Tab: 4", "Ln: 0, Col: 0"]
		forWigets(lis, self._theme.callw, self.cuerpo, "buttombar.lab",
		          "right")
		return super().create("bottom")

def test():
	from tkinter import Tk, Frame
	ven = Tk()
	ven.title("Barra Inferior")
	BB = ButtomBar(Frame(ven), ven, None)
	BB.createButtombar()
	ven.mainloop()

if __name__ == "__main__":
	test()
	