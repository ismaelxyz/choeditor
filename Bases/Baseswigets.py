﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright © 2020 Ismael Belisario

This file is part of Choeditor.

Choeditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Choeditor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Choeditor.  If not, see <https://www.gnu.org/licenses/>.
"""

from tkinter import Scrollbar

class Style(dict):

    def __init__(self):
        self.add = self.update
        # self.clear clear dict whit style
    
    def changeTheme(self, theme):
        self.clear()
        self.update(theme)
    
    def update(self, theme):

        if isinstance(theme, dict):
            super().update(theme)

        else:
            print(theme)
            input("Error:")
            raise ValueError

from tkinter import Label, Frame, LabelFrame, Entry, Button

class CreatedWigets(Style):

    def __init__(self):
        super().__init__()
        self.stats = {"Hover":["<Enter>", "<Leave>"], 
                      "Focus":["<FocusIn>", "<FocusOut>"], 
                      "Selec":["<ButtonPress-1>", "<ButtonRelease-1>"]}
        self.valu = [n for n in self.stats]
        self.callw = self.callWigets
        self.creados = {}
        # self.__categorias = [""] #Categorias
        # individual grupal global
    
    def frame(self, master, styl, cnf={}, **kw):
        
        fra = Frame(master, height=styl["height"], width=styl["width"],
        relief=styl["relief"], bg=styl["bg"], bd=styl["bd"])

        return fra

    def __interactive(self, enu, y, wig, styl):
        wig.bind(self.stats[self.valu[enu]][0],
                 lambda x: [[wig.__setitem__(b[:-5], styl[b]) for b in y]])
        wig.bind(self.stats[self.valu[enu]][1],
                 lambda x: [[wig.__setitem__(b[:-5], styl[b[:-5]]) for b in y]])
    
    def createInteraction(self, lab, styl):
        charger = [[],[],[]]

        for site in ["bg", "fg", "bd", "font"]:
            for num, st in enumerate(self.stats):
                if site + st in styl:
                    charger[num].append(site + st)

        for num, aplict in enumerate(charger):
            if aplict:
                self.__interactive(num, aplict, lab, styl)

    def label(self, master, styl, cnf={}, **kw):
        
        lab = Label(master, height=styl["height"], width=styl["width"], fg=styl["fg"],
        relief=styl["relief"], bg=styl["bg"], bd=styl["bd"], font=styl["font"], **kw)

        self.createInteraction(lab, styl)

        return lab
     
    def labelFrame(self, master, styl, cnf={}, **kw):
        pass
      
    def entry(self, master, styl, cnf={}, **kw):
        ent = Entry(master, bd=styl["bd"], bg=styl["bg"], font=styl["font"],
              relief=styl["relief"], borderwidth=styl["borderwidth"],
              fg=styl["fg"], selectforeground=styl["selectforeground"],
              selectbackground=styl["selectbackground"], width=styl["width"], **kw)
        """
        exportselection, foreground, highlightbackground,
        highlightcolor, highlightthickness, insertbackground,
        insertborderwidth, insertofftime, insertontime, insertwidth,
        invalidcommand, invcmd, 
        selectborderwidth, , takefocus,
        textvariable, validate, validatecommand, vcmd, ,
        """

        return ent
        
    
    def text(self):
        pass
    
    def menu(self):
        pass
    
    def buttonMenu(self):
        pass
    
    def button(self, master, styl, cnf={}, **kw):
        but = Button(master, bd=styl["bd"], bg=styl["bg"], font=styl["font"],
              relief=styl["relief"], borderwidth=styl["borderwidth"],
              fg=styl["fg"], activeforeground=styl["activeforeground"],
              activebackground=styl["activebackground"], width=styl["width"], **kw)
        
        self.createInteraction(but, styl)

        return but

    def callWigets(self, master, name, rturn=True, cnf={}, **kw):
        obj = None
        styl = self[name]  # estilo del tema elegido.
        if ".tk" in name:
            for x in styl:
                master[x] = styl[x]
            
            return True

        for x in ["height", "width", "fg", "relief", "bd", "borderwidth",
                  "selectbackground", "selectforeground", "activeforeground",
                  "activebackground"]:

                if x not in styl:
                    styl.update({x:None})
                    
        if "font" not in styl:
            styl.update({"font":("", 10, "normal")})
        
        if ".fra" in name:
            obj = self.frame(master, styl, rturn, **kw)

        elif ".lab" in name:
            obj = self.label(master, styl, rturn, **kw)
        
        elif ".ent" in name:
            obj = self.entry(master, styl, rturn, **kw) 
        
        elif ".but" in name:
            obj = self.button(master, styl, rturn, **kw)
        
        self.creados.update(name=obj)
        
        if rturn:
            return obj
        
        obj.pack()
    
    def changeTheme(self, theme):
        super().changeTheme(theme)
        for cont in self.creados:
            for x in self[cont]:
                self.creados[cont][x] = self[cont]


class AutoScrollbar(Scrollbar, object):

    def __init__(self, parent, **opts):
        super(AutoScrollbar, self).__init__(parent, **opts)
        self.hidden = None

    # a scrollbar that hides itself if it's not needed
    # only works if you use the grid geometry manager
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
            self.hidden = True
        else:
            self.grid()
            self.hidden = False
        super(AutoScrollbar, self).set(lo, hi)

    def pack(self, **kw):
        raise Exception("cannot use pack with this widget")

    def place(self, **kw):
        raise Exception("cannot use place with this widget")

def forWigets(lis, func, master, text, side, space=5, n=1):
    # for bucle
    count = space
    for x, y in enumerate(lis):
        func(master, f"{text}{x+n}", text=y).pack_configure(side=side,
                                                            padx=count)
        if count == 0:
            count += space

        else:
            count -= space

class BaseBars(object):
    def __init__(self, cuerpo, theme, **kw):
        self._theme = theme
        self.cuerpo = cuerpo
        self.master = self.cuerpo.master
    
    def create(self, side):
        self.cuerpo.pack(fill="x", side=side)
        return self.cuerpo