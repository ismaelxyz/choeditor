#!/usr/bin/env python
# -*- coding: utf-8 -*-

from EditJson import viewFileJsom, writeFileJsom, viewJsonZip

class LoadExtensions:

    def __init__(self):
        self.thisdir = __import__("Constans").path(__file__, "Extensions")
        self.loadEstencion()
    
    def loadEstencion(self):
        posible_estencion = self.searchEstencions(__import__("os")\
                                            .listdir(self.thisdir))
        self.__estencion = self.inspecionarExtencion(posible_estencion)
    
    def getEstencion(self):
        return self.__estencion

    def searchEstencions(self, elemens):
        from EditJson import viewFileJsom
        from os.path import join, isfile
        exten = {}

        for elemen in [join(self.thisdir, x, "info.json") for\
                    x in elemens if isfile(join(self.thisdir, x,
                                                "info.json"))]:
            read = viewFileJsom(elemen)
            
            if read is not None:
                try:
                    exten.update({read["data"]["name"]:read}) # {"name":red}
                except KeyError:# as identifier:
                    pass
        
        return exten

    def inspecionarExtencion(self, posible_e):
        from Constans import confirmar
        acumulator = {}
        exen = [x for x in posible_e]
        for x in exen:
            if confirmar(one=self.revisarIniData(posible_e[x])):
                acumulator.update({x:posible_e[x]})
        
        return acumulator
    
    def revisarBase(self, search, objec):
        if isinstance(objec, dict):
            for x in search:
                if x not in objec:
                    return False
            return True
        return False
    
    def revisarIniData(self, este):
        eleme = ["data", "files"]  # file = dirname + basename
        if isinstance(este, dict) and self.revisarBase(eleme, este):
            if not self.revisarBase(["author", "copyright", "version", "licence",
               "prefix", "description", "url"], este["data"]) or not este["files"]:
                return False
            return True
        return False
    
    def revisarData(self, data):
        pass
    
    def revisarActivate(self, active):
        # TODO FIXME XXX
        # Guardar las formas de inicio
        return True
    
"""

 "activate":{
             "recivir_clase": null,
             "shorcut": "Alt-c",
             "requisitos": null,
             "master": "main.win"
            },
"""

class Configure:

    def __init__(self):
        self.load_extensions = LoadExtensions()
        self.getEstencion = self.load_extensions.getEstencion
        
        self.load_all()

    def load_all(self):
        self.loadFileConfig()
        self.loadTheme()
        self.loadExtensions()
        
    def loadExtensions(self):
        self.load_extensions.loadEstencion()
        self.__extencios = self.load_extensions.getEstencion()

    def loadFileConfig(self):
        configure = viewFileJsom('Bases/Jsons/config.json')
        if configure is not None:
            self.__configure = configure
            return True
        
        else:
            return False

    def loadTheme(self, name=None):
        if name is None:
        
            self.__info_theme = viewJsonZip(self.__configure["theme path"],
                                            "info.json")
            _theme = {}

            for _file in self.__info_theme["FILES"]:
                _theme.update(viewJsonZip(self.__configure["theme path"], 
                                        _file))
            self.__path_theme = self.__configure["theme path"]
            self.__theme = _theme
        
        else:
            configure = viewFileJsom('Bases/Jsons/config.json')
            new_theme = configure["theme path"][:12] + theme
            configure["theme path"] = new_theme
            writeFileJsom('Bases/Jsons/config.json', configure)

    def getTheme(self):
        return self.__theme

    def getInfoTheme(self):
        info = self.__info_theme.copy()
        info.update({"theme path":self.__path_theme})

        return info

def test():
    cc = Configure()
    print(cc.getInfoTheme())

if __name__ == "__main__":
    test()
