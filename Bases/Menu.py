#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright © 2020 Ismael Belisario

This file is part of Choeditor.

Choeditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Choeditor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Choeditor.  If not, see <https://www.gnu.org/licenses/>.
"""

from Baseswigets import BaseBars, forWigets

class Menu(BaseBars):
    def __init__(self, cuerpo, theme, **kw):
        super().__init__(cuerpo, theme, **kw)
    
    def create(self):
        lis = ["Archivo", "Editar", "Ver", "Ir", "Ejecutar", "Terminal", "Ayuda"]
        forWigets(lis, self._theme.callw, self.cuerpo, "menu.lab", "left", n=2)
        return super().create("top")
    
    def action(self):
        normal = ["Nuevo archivo", "Abrir archivo...", "Guardar", 
                  "Guardar Como", "Nueva ventana", "Abrir carpeta...",
                  "Abrir reciente>", "Preferencias>", "Cerrar ventana",
                  "Cerrar pestaña", "Salir"]

    def separator(self):
        pass

"""
from tkinter import messagebox as mb
from tkinter import filedialog as fd
from io import open

#Variables de control
ruta = "" #Variable para verificar archivo
g = "" #Variable para verificar borrado

#Funciones del Menu
	#Archivo
def nuevo():
	if(tx.edit_modified() == 0):
		tx.delete(1.0, END)
	elif(tx.edit_modified() == 1):
		r = mb.askquestion("Guardar", "¿Quiere guardar el archivo?")
		if(r == 'yes'):
			guardar() #Verificar que no guarde si regresa cancelar BORRAR
			if(g != None):
				tx.delete(1.0, END)
				tx.edit_modified(False)
		else:
			tx.delete(1.0, END)
			tx.edit_modified(False)

def abrir():
	nuevo()
	global ruta
	try:
		ruta = fd.askopenfilename(title="Abrir",
		                          filetypes=(("Texto plano", "*.txt"), 
											 ("Python", "*.py"), 
											 ("C", "*.c"),
											 ("GIT", "*.gitignore"),
											 ("C++", "*.cpp"),
											 ("JSON", "*.json"),
											 ("JavaScript", "*.js"),
											 ("HTML", "*.html"),
											 ("CSS", "*.css"),
											 ("LESS", "*.less"),
											 ("Todos los ficheros", "*.*")))
	except TclError:
		mb.showwarning("Error", "Ocurrio un error y no se puede abrir el archivo")
	if(ruta != ""):
		archivo = open(ruta, "r+")
		contenido = archivo.read()
		tx.delete(1.0, END)
		tx.insert(INSERT, contenido)
		archivo.close()
		tx.edit_modified(False)

def guardar():
	global ruta
	if(tx.edit_modified() == 0):
		mb.showinfo("Información", "El archivo XXX a sido guardado.")
	elif(tx.edit_modified() == 1):
		if(ruta != ""):
			contenido = tx.get(1.0, END)
			archivo = open(ruta, "w+")
			archivo.write(contenido)
			archivo.close()
			tx.edit_modified(False)
			tx.edit_reset()
		elif(ruta == ""):
			guardarComo()
# bad Macintosh file type

def guardarComo():
	archivo = fd.asksaveasfile(title="Guardar como", mode="w+", defaultextension=".py",
	                           filetypes = (("Texto plano", "*.txt"), 
										    ("Python", "*.py"), 
										    ("C", "*.c"),
										    ("GIT", "*.gitignore"),
										    ("C++", "*.cpp"),
										    ("JSON", "*.json"),
										    ("JavaScript", "*.js"),
										    ("HTML", "*.html"),
										    ("CSS", "*.css"),
										    ("LESS", "*.less"),
										    ("Todos los ficheros", "*.*")))
	global g
	g = archivo
	if archivo is not None:
		contenido = tx.get(1.0, END)
		archivo.write(contenido)
		archivo.close()

	#Editar
def undo():
	tx.event_generate("<<Undo>>")
def redo():
	tx.event_generate("<<Redo>>")
def cortar():
	tx.event_generate("<<Cut>>")
def copiar():
	tx.event_generate("<<Copy>>")
def pegar():
	tx.event_generate("<<Paste>>")

	#Ayuda
def Ade():
	vHija = Toplevel(ven)
	vHija.geometry("300x50+500+500")
	Label(vHija,text="Procesador de tx v1.0").pack()
	Label(vHija,text="© 2020 - Ismael Belisario").pack()

#Manejar cuando se cierra el programa
def cerrar():
	if(tx.edit_modified() != 0):
		r = mb.askquestion("Guardar", "¿Quiere guardar el archivo antes de salir?")
		if(r == 'yes'):
			guardar() #Verificar que no guarde si regresa cancelar BORRAR
			if(g != None):
				ven.destroy()
		else:
			ven.destroy()
	else:
		ven.destroy()


#Cosas para implementar
'''
widget.edit_reset() #Para limpiar la pila de undo cuando se guarda el documento
widget.event_info() #Para ver que eventos tiene por defecto

Código sin usar para crear copiar
try:
	copia = tx.selection_get() #VERIFICAR
except TclError:
	mb.showinfo("Información", "No hay nada en el portapapeles")
	print(tx.event_info()	)
'''

menubar = Menu(ven)
ven.config(menu=menubar)

archivo = Menu(menubar, tearoff=0)

archivo.add_command(label="Nuevo", command=nuevo)
archivo.add_command(label="Abrir", command=abrir)
archivo.add_command(label="Guardar", command=guardar)
archivo.add_command(label="Guardar como", command=guardarComo)
archivo.add_separator()
archivo.add_command(label="Cerrar", command=ven.quit)

# Agregar comandos al submenu Editar
editar = Menu(menubar, tearoff=0)
editar.add_command(label="Deshacer", command=undo)
editar.add_command(label="Rehacer", command=redo)
editar.add_command(label="Cortar", command=cortar)
editar.add_command(label="Copiar", command=copiar)
editar.add_command(label="Pegar", command=pegar)

# Agregar comandos al submenu Ayuda
ayuda = Menu(menubar,tearoff=0)
ayuda.add_command(label="Acerca de", command=Ade)

# Agregar los submenus al Menu
menubar.add_cascade(label="Archivo", menu=archivo)
menubar.add_cascade(label="Editar", menu=editar)
menubar.add_cascade(label="Ayuda", menu=ayuda)

fontt = ["Andalus", 10, "normal"]


if __name__ == "__main__":
	ven.protocol("WM_DELETE_WINDOW", cerrar)
	ven.mainloop()
"""
