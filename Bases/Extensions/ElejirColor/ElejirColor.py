# !/usr/bin/env python
# -*- coding: utf-8 -*-

# Autor: Ismael Belisario
# Nombre Clave: CHOLP1
# Para más ejecicios visite: https://github.com/ismaelxyz
# Para contactar al autor: <ismaelbeli.com@gmail.com>

"""
Copyright © 2020 Ismael Belisario

This Elejir Color is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Elejir Color is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Elejir Color.  If not, see <https://www.gnu.org/licenses/>.
"""

from tkinter import Tk, StringVar, Toplevel
from tkinter.colorchooser import askcolor
from tkinter import TclError
#from Bases.Style import CreatedWigets
# Solo ejecutar con el sistema pricipal
from Baseswigets import CreatedWigets
from EditJson import viewFileJsom, writeFileJsom
from Constans import path

class ViewColor(Toplevel):
    """docstring for ."""

    def __init__(self, master):
        super().__init__(master)
        self.makewigets = CreatedWigets()
        self.callw = self.makewigets.callw
        self.thisDir = path(__file__)
        self.file_save_color = path(__file__, "SaveColors.json")
        self.file_theme = path(__file__, "Style.json")
        self.makewigets.add(viewFileJsom(self.file_theme))
        self.callw(self, ".tk")

        self.protocol("WM_DELETE_WINDOW", self.exitApp)
        self.__color = StringVar()
        self.__color2 = StringVar()
        self.__selected = self.__selected2 = 0

        self.modelWindow()
        loaded_data = viewFileJsom(self.file_save_color)

        if loaded_data is not None:
            try:
                for xy in loaded_data["search"]:
                    self.listColors(xy)

                for xy in loaded_data["choce"]:
                    self.listColors2(xy)

                self.__selected += loaded_data["selected"]
                self.moveSelected(self.__selected, self.listColors(),
                                self.__color, self.palete)
                self.__selected += loaded_data["selected2"]
                self.moveSelected(self.__selected2, self.listColors2(),
                                self.__color2, self.palete2)

            except IndexError as ErrorDeIndice:
                pass

    def searchColor(self):
        my_color = askcolor()
        print(my_color, "mnb")
        if my_color[1] is not None:
            self.palete["bg"] = my_color[1]
            self.__color.set("Hex: %s" % my_color[1])
            self.listColors(my_color[1])
            print(self.listColors())

    def changeColor(self):
        valor = self.__color2.get()

        if ":" in valor:
            valor = valor[valor.index(":")+1:]

        clear = ""
        for v in valor:
            if v.lower().isalpha() or v.isdigit() or v == "#" and clear == "":
                clear += v.lower()
        valor = clear

        def __change(vol):
            try:
                if vol is not None:
                    print(vol, "cc")
                    self.palete2["bg"] = vol
                    self.__color2.set("Hex: %s" % vol)
                    self.listColors2(vol)
                return True

            except TclError:
                return False

        if not __change(valor):
            vlr = ""
            for x in valor:
                if x.isdigit() or x.lower() in ("a", "b", "c", "d", "e", "f"):
                    vlr += x.lower()
                    if len(vlr) == 6:
                        if "x" not in vlr:
                            vlr = "#" + vlr

                            if not __change(vlr):
                                self.palete2["bg"] = "white"
                                self.__color2.set("Color desconocido se cambia: Blanco")

    def modelStructure(self, text, command, textvar, num):
        go_to = {"Buscar Color":[lambda:self.moveSearch("-"),
                                 lambda:self.moveSearch()],
                 "Cambiar Color":[lambda:self.moveChange("-"),
                                  lambda:self.moveChange()]}

        fr = self.callw(self, ".fra1")
        fr.pack()

        fr1 = self.callw(fr, ".fra1")
        fr1.pack()

        fr2 = self.callw(fr, ".fra1")
        fr2.pack(ipadx=75)

        self.callw(fr1, ".but1", True, text=text, command=command)\
            .pack(pady=5, side="right", ipadx=num)
        self.callw(fr1, ".ent1", True, textvariable=textvar).pack(pady=3, padx=2, side="right")
        self.callw(fr2, ".but1", True, text="Anterior",
                                     command=go_to[text][0]).pack(side="left")
        self.callw(fr2, ".but1", True, text="Siguiente", command=go_to[text][1]).pack(side="left",
                                                                  padx=4)

        return fr

    def moveSearch(self, operator="+"):
        if operator == "+" and len(self.listColors()) -1 > self.__selected:
            self.__selected += 1

        if operator == "-" and self.__selected > 0:
            self.__selected -= 1
        self.moveSelected(self.__selected, self.listColors(), self.__color,
                          self.palete)

    def moveChange(self, operator="+"):
        if operator == "+" and len(self.listColors2()) -1 > self.__selected2:
            self.__selected2 += 1

        if operator == "-" and self.__selected2 > 0:
            self.__selected2 -= 1
        self.moveSelected(self.__selected2, self.listColors2(), self.__color2,
                          self.palete2)

    def moveSelected(self, locate, lis, textvar, palete):
        print(locate)
        textvar.set("Hex: %s" % lis[locate])
        palete["bg"] = lis[locate]

    def modelWindow(self):  # Modelar ventana
        self.modelStructure("Buscar Color", self.searchColor, self.__color, 4)\
                               .pack_configure(pady=7, ipadx=50)

        self.modelStructure("Cambiar Color", self.changeColor,
                               self.__color2, 2)\
                               .pack_configure(pady=5, ipadx=20)

        fr = self.callw(self, ".fra1")
        self.callw(fr, ".lab1", True, text="Hex\t\t\tRGB -> Hex").pack()
        self.palete = self.callw(fr, ".lab2")
        self.palete2 = self.callw(fr, ".lab2")
        self.palete.pack(padx=4, side="left")
        self.palete2.pack(padx=4, side="left")
        fr.pack(pady=4, padx=10)

    def saveFile(self):
        writeFileJsom(self.file_save_color, {"selected": self.__selected,
                                             "selected2": self.__selected2,
                                             "search": self.listColors(),
                                             "choce": self.listColors2()})

    def listColors(self, new_var=None, var=[]):
        if new_var is not None:
            if new_var not in var:
                var.append(new_var)

        else:
            return var

    def listColors2(self, new_var=None, var=[]):
        if new_var is not None:
            if new_var not in var:
                var.append(new_var)

        else:
            return var

    def exitApp(self):
        print("In exit")
        self.saveFile()
        self.destroy()

print("Eleji:", __file__)
if __name__ == "__main__":
    ven = Tk(className=" Buscar un Color")
    asd = ViewColor(ven)
    ven.mainloop()
