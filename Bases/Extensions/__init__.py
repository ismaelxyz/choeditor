﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (C) 2020 Ismael Belisario

This file is part of Choeditor.

Choeditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Choeditor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Choeditor.  If not, see <https://www.gnu.org/licenses/>.
"""
