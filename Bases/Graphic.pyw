#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Ismael Belisario
# Nombre Clave: CHOGR1
# Para más ejecicios visite: https://github.com/ismaelxyz
# Para contactar al editor: <ismaelbeli.com@gmail.com>

# Copyright © 2020 Ismael Belisario

"""
Copyright © 2020 Ismael Belisario

This file is part of Choeditor.

Choeditor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This Choeditor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this Choeditor.  If not, see <https://www.gnu.org/licenses/>.
"""
from tkinter import Tk, PhotoImage
from Config import Configure
from Baseswigets import CreatedWigets, forWigets
from Buttombar import ButtomBar
from Menu import Menu

#.strip() Eliminar espacios
class Windows(Tk):

    def __init__(self, theme, name=" Choeditor"):
        super().__init__(className=name)
        self.title(name.strip())
        self.__theme = theme
        self.callw = theme.callWigets
        self.__logo = PhotoImage(file="Bases/Images/lo.png")
        self.__wigets = {}

        self.call('wm', 'iconphoto', self, self.__logo)
        self.menu_bar = self.makeMenu()
        from tkinter import Frame
        print(type(self.menu_bar), isinstance(self.menu_bar, Frame))
        self.title_x = self.callw(self.menu_bar, "menu.lab1", text="Choeditor - Sín archivos")
        self.title_x.pack(padx=10, ipadx=5)
    
    def setTitle(self, title):
        print(self.title_x.xroot())
        self.title_x["text"] = title

    def makeMenu(self):
        m = self.__theme.callw(self, "menu.fra", image=self.__logo)
        m = Menu(m, self.__theme)

    def makelabels(self, master, name, cnf={}, **kw):

        bm = self.callw(master, name, True, **kw)
        bm.pack()

        return bm

cf = Configure()
#print(cf.getTheme())
#print(cf.getEstencion())

ct = CreatedWigets()
ct.add(cf.getTheme())

def testEs():
    from Extensions.ElejirColor.ElejirColor import ViewColor
    ven = Tk()
    ViewColor(ven)
    ven.mainloop()
#testEs()

def test():
    pass
#test()

def testB():
	from tkinter import Frame
	wi = Windows(ct)

	BB = ButtomBar(ct.callw(wi, "buttombar.fra"), ct)
	BB.create()
	wi.mainloop()
testB()