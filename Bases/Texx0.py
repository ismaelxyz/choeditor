#!/usr/bin/env python3

#from tkinter import (Text, Tk, Frame, LabelFrame, Scrollbar, Canvas, Menu,
#                     Menubutton, Label, END, INSERT, TclError, Toplevel)
from Baseswigets import BaseBars
class CText(object):
    def __init__(self, cuerpo, theme, **kw):
	    super().__init__(cuerpo, theme, **kw)
	
    def create(self):
        pass
        #return super().create("bottom")


#Variables de control
ruta = "" #Variable para verificar archivo
g = "" #Variable para verificar borrado

#Funciones del Menu
	#Archivo
def nuevo():
	if(tx.edit_modified() == 0):
		tx.delete(1.0, END)
	elif(tx.edit_modified() == 1):
		r = mb.askquestion("Guardar", "¿Quiere guardar el archivo?")
		if(r == 'yes'):
			guardar() #Verificar que no guarde si regresa cancelar BORRAR
			if(g != None):
				tx.delete(1.0, END)
				tx.edit_modified(False)
		else:
			tx.delete(1.0, END)
			tx.edit_modified(False)

def abrir():
	nuevo()
	global ruta
	try:
		ruta = fd.askopenfilename(title="Abrir",
		                          filetypes=(("Texto plano", "*.txt"), 
											 ("Python", "*.py"), 
											 ("C", "*.c"),
											 ("GIT", "*.gitignore"),
											 ("C++", "*.cpp"),
											 ("JSON", "*.json"),
											 ("JavaScript", "*.js"),
											 ("HTML", "*.html"),
											 ("CSS", "*.css"),
											 ("LESS", "*.less"),
											 ("Todos los ficheros", "*.*")))
	except TclError:
		mb.showwarning("Error", "Ocurrio un error y no se puede abrir el archivo")
	if(ruta != ""):
		archivo = open(ruta, "r+")
		contenido = archivo.read()
		tx.delete(1.0, END)
		tx.insert(INSERT, contenido)
		archivo.close()
		tx.edit_modified(False)

def guardar():
	global ruta
	if(tx.edit_modified() == 0):
		mb.showinfo("Información", "El archivo XXX a sido guardado.")
	elif(tx.edit_modified() == 1):
		if(ruta != ""):
			contenido = tx.get(1.0, END)
			archivo = open(ruta, "w+")
			archivo.write(contenido)
			archivo.close()
			tx.edit_modified(False)
			tx.edit_reset()
		elif(ruta == ""):
			guardarComo()
# bad Macintosh file type

def guardarComo():
	archivo = fd.asksaveasfile(title="Guardar como", mode="w+", defaultextension=".py",
	                           filetypes = (("Texto plano", "*.txt"), 
										    ("Python", "*.py"), 
										    ("C", "*.c"),
										    ("GIT", "*.gitignore"),
										    ("C++", "*.cpp"),
										    ("JSON", "*.json"),
										    ("JavaScript", "*.js"),
										    ("HTML", "*.html"),
										    ("CSS", "*.css"),
										    ("LESS", "*.less"),
										    ("Todos los ficheros", "*.*")))
	global g
	g = archivo
	if archivo is not None:
		contenido = tx.get(1.0, END)
		archivo.write(contenido)
		archivo.close()

	#Editar
def undo():
	tx.event_generate("<<Undo>>")
def redo():
	tx.event_generate("<<Redo>>")
def cortar():
	tx.event_generate("<<Cut>>")
def copiar():
	tx.event_generate("<<Copy>>")
def pegar():
	tx.event_generate("<<Paste>>")

	#Ayuda
def Ade():
	vHija = Toplevel(ven)
	vHija.geometry("300x50+500+500")
	Label(vHija,text="Procesador de tx v1.0").pack()
	Label(vHija,text="© 2020 - Ismael Belisario").pack()

#Manejar cuando se cierra el programa
def cerrar():
	if(tx.edit_modified() != 0):
		r = mb.askquestion("Guardar", "¿Quiere guardar el archivo antes de salir?")
		if(r == 'yes'):
			guardar() #Verificar que no guarde si regresa cancelar BORRAR
			if(g != None):
				ven.destroy()
		else:
			ven.destroy()
	else:
		ven.destroy()


#Cosas para implementar
'''
widget.edit_reset() #Para limpiar la pila de undo cuando se guarda el documento
widget.event_info() #Para ver que eventos tiene por defecto

Código sin usar para crear copiar
try:
	copia = tx.selection_get() #VERIFICAR
except TclError:
	mb.showinfo("Información", "No hay nada en el portapapeles")
	print(tx.event_info()	)
'''

menubar = Menu(ven)
ven.config(menu=menubar)

archivo = Menu(menubar, tearoff=0)

archivo.add_command(label="Nuevo", command=nuevo)
archivo.add_command(label="Abrir", command=abrir)
archivo.add_command(label="Guardar", command=guardar)
archivo.add_command(label="Guardar como", command=guardarComo)
archivo.add_separator()
archivo.add_command(label="Cerrar", command=ven.quit)

# Agregar comandos al submenu Editar
editar = Menu(menubar, tearoff=0)
editar.add_command(label="Deshacer", command=undo)
editar.add_command(label="Rehacer", command=redo)
editar.add_command(label="Cortar", command=cortar)
editar.add_command(label="Copiar", command=copiar)
editar.add_command(label="Pegar", command=pegar)

# Agregar comandos al submenu Ayuda
ayuda = Menu(menubar,tearoff=0)
ayuda.add_command(label="Acerca de", command=Ade)

# Agregar los submenus al Menu
menubar.add_cascade(label="Archivo", menu=archivo)
menubar.add_cascade(label="Editar", menu=editar)
menubar.add_cascade(label="Ayuda", menu=ayuda)

fontt = ["Andalus", 10, "normal"]
fr2 = Frame(ven)
Label(fr2, text="Python", font=fontt).pack(side="right")
Label(fr2, text="UTF-8", font=fontt).pack(side="right", padx=5)
Label(fr2, text="Tab: 4", font=fontt).pack(side="right", padx=5)
Label(fr2, text="Ln: 0, Col: 0", font=fontt).pack(side="right")
fr2.pack(side="bottom", fill="x")

fr = Frame(ven, bg="red", width=20, height=5)
fr.pack(fill="y", expand=1)
fr.rowconfigure(2, weight=1)
fr.columnconfigure(2, weight=1)

fr.yscrollbar = AutoScrollbar(fr)
#fr.xscrollbar = AutoScrollbar(fr, orient="horizontal")

#ops = {'yscrollcommand': fr.yscrollbar.set}
# 'xscrollcommand': fr.xscrollbar.set}

fr.yscrollbar.grid(row=0, column=1, sticky="nsew", rowspan=3)

#fr.xscrollbar.grid(row=2, column=0)
tx = Text(fr, yscrollcommand=fr.yscrollbar.set)#, **ops

tx.config(highlightthickness=0, bd=0)
tx.grid(row=0, column=0, sticky="nsew", rowspan=3)

fr.yscrollbar.config(command=tx.yview)
#fr.xscrollbar.config(command=tx.xview)

ven.protocol("WM_DELETE_WINDOW", cerrar)
ven.mainloop()


# column, columnspan, in, ipadx, ipady, padx, pady, row, rowspan, or sticky
# must be n, ne, e, se, s, sw, w, nw, or center
# MATH Case
# text.bind("<<close-all-windows>>", self.flist.close_all_callback)

# Divide the width of the Text widget by the font width,
# which is taken to be the width of '0' (zero).
# http://www.tcl.tk/man/tcl8.6/TkCmd/text.htm#M21

"""
if __name__ == "__main__":
    from unittest import main
    main('idlelib.idle_test.test_mainmenu', verbosity=2, exit=False)

    from idlelib.idle_test.htest import run
    run(_multi_call)
"""
# Ver Curso Python FES Acatlán