#!/usr/bin/env python
# -*- coding: utf-8 -*-

def viewFileJsom(vfile):
    #from os.path import isfile
    is_file = __import__("os").path.isfile(vfile)

    if not is_file:
        with open(vfile, "w") as fil:
            fil.close()
        return None

    elif is_file:
        with open(vfile, "r") as fil:
            #from json import load
            from json.decoder import JSONDecodeError
            try:
                return [__import__("json").load(fil), fil.close()][0]
            except JSONDecodeError:
                fil.close()
                return None

def viewJsonZip(nameZip, namefile):
    
    with __import__("zipfile").ZipFile(nameZip, "r") as myzip:
        with myzip.open(namefile, "r") as myfile:

             return [__import__("json").load(myfile), myfile.close()][0] 

def writeFileJsom(file, data):
    with open(file, "w") as fil:
        __import__("json").dump(data, fil)
        fil.close()